import React from 'react'
import ReactDOM from 'react-dom'

const App = () => {
  const buttonText = 'Click and Submit'

  return (
    <div>
      <label className="label" htmlFor="name">
        Enter Name
      </label>
      <input id="name" />
      <button style={{ backgroundColor: 'blue', color: 'white' }}>
        {buttonText}
      </button>
    </div>
  )
}
// export default App

ReactDOM.render(<App />, document.querySelector('#root'))
