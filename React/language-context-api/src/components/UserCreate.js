import React from 'react'
import Field from './Field'
import Button from './Button'
import ButtonConsumer from './ButtonConsumer'

// User consumers when there are multiple context objects

const UserCreate = () => {
  return (
    <div className="ui form">
      <Field />
      <Button />
      <ButtonConsumer />
    </div>
  )
}
export default UserCreate
