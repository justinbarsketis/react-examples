import React from 'react'
import ReactDOM from 'react-dom'
import SeasonDisplay from './SeasonDisplay'
import Loader from './Loader'

class App extends React.Component {
  //   constructor(props) {
  //     super(props)
  //     this.state = { lat: null, long: null, errorMessage: '' }
  //   }
  // Constructor without anything except state can be set like below

  state = { lat: null, long: null, errorMessage: '' }
  componentDidMount() {
    window.navigator.geolocation.getCurrentPosition(
      (position) =>
        this.setState({
          lat: position.coords.latitude,
          lon: position.coords.longitude
        }),
      (err) => this.setState({ errorMessage: err.message })
    )
  }

  renderContent() {
    if (this.state.errorMessage && !this.state.lat) {
      return <div>Error: {this.state.errorMessage}</div>
    }
    if (!this.state.errorMessage && this.state.lat) {
      return (
        <SeasonDisplay
          lat={this.state.lat}
          lon={this.state.long}
          err={this.errorMessage}
        />
      )
    }
    return <Loader message="Please accept location request" />
  }
  render() {
    return <div>{this.renderContent()}</div>
  }
}

ReactDOM.render(<App />, document.querySelector('#root'))
