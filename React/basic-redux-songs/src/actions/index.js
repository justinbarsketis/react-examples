// action creator
export const selectSong = (song) =>
  // must return an action
  {
    return {
      type: 'SONG_SELECTED',
      payload: song
    }
  }
