import { combineReducers } from 'redux'

const songsReducer = () => {
  return [
    { title: 'Jump', duration: '4:05' },
    { title: 'Lets go', duration: '2:05' },
    { title: 'Hi Five', duration: '6:05' },
    { title: 'There we went', duration: '1:05' }
  ]
}

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === 'SONG_SELECTED') {
    return action.payload
  } else {
    return selectedSong
  }
}

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer
})
