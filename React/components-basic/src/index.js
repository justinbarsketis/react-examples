import React from 'react'
import ReactDOM from 'react-dom'
import CommentDetail from './components/CommentDetail'
import faker from 'faker'
import ApprovalCard from './components/ApprovalCard'

const App = () => {
  return (
    <div>
      <ApprovalCard>
        <CommentDetail
          img={faker.image.image()}
          author="Sam"
          date="Today at 6pm"
          message="Hello"
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          img={faker.image.image()}
          author="Justin"
          date="Yesterday at 6pm"
          message="Never enough"
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          img={faker.image.image()}
          author="Tucker"
          date="12/16/2022"
          message="Fun times sometimes"
        />
      </ApprovalCard>
    </div>
  )
}
// export default App

ReactDOM.render(<App />, document.querySelector('#root'))
