const postReducer = (state = [], action) => {
  switch (action.type) {
    case 'FETCH_POSTS':
      return action.payload
    default:
      return state // return default state if the action type isnt relevant
  }
}
export default postReducer
