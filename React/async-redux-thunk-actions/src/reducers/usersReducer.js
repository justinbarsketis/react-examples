const usersReducer = (state = [], action) => {
  switch (action.type) {
    case 'FETCH_USER':
      return [...state, action.payload]
    default:
      return state // return default state if the action type isnt relevant
  }
}
export default usersReducer
