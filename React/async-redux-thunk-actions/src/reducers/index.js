import { combineReducers } from 'redux'
import postReducer from './postReducer'
import usersReducer from './usersReducer'
export default combineReducers({ posts: postReducer, users: usersReducer })

// Reducer Rules
// Must return any value except undefined, even at initiation, return values of null is okay
// Produces state or data to be used in your app using only the previous state and action dispatched
// Must not reach out of its own function to decide what value to return, reducers are pure
// Must not mutate its input state, you technically can but if you hit the corner case it wont update app
// Edge case is if you modify an array, object is the same so hasState changed check is falsely true

// proper state actions
// removing from array state.filter(e => e !== 'hi')
// adding  to  array [..state, 'hi]
// replacing in array state.map(el => el === 'hi' ? 'bye: el)
// updating a property object {...state, name:'Stan'}
// adding a property to object {...state, age: 30}
// removing a property from object {...state, age: undefined} (not the best) || _.omit(state, 'age) (lodash library)
// .find === true, to find element in array, returns item when true
