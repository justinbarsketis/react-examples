import { get } from 'lodash'
import json from '../apis/json'
import _ from 'lodash'

// export const fetchPostsAndUsers = () => async (dispatch, getState) => {
//   await dispatch(fetchPosts())
//   const userIds = _.uniq(_.map(getState().posts, 'userId')) // use lodash to get unique ids, from their lodash map function
//   userIds.forEach((id) => dispatch(fetchUser(id))) // no need for await as there is no logic after this, await doesnt work for each statement either
// }
export const fetchPostsAndUsers = () => async (dispatch, getState) => {
  await dispatch(fetchPosts())

  _.chain(getState().posts)
    .map('userId')
    .uniq()
    .forEach((id) => dispatch(fetchUser(id)))
    .value()
  //using chain instead of example above
}
// need to remove fetch from userHeader to complete this fetch and users
export const fetchPosts = () => async (dispatch) => {
  //can add getState
  const resp = await json.get('/posts')
  dispatch({ type: 'FETCH_POSTS', payload: resp.data })
}

// using memo from lodash
export const fetchUser = (id) => async (dispatch) => {
  //can add getState
  const resp = await json.get(`/users/${id}`)
  dispatch({ type: 'FETCH_USER', payload: resp.data })
}

// // using memo from lodash
// export const fetchUser = (id) => (dispatch) => _fetchUser(id, dispatch) //memoized function
// //can add getState

// const _fetchUser = _.memoize(async (id, dispatch) => {
//   const resp = await json.get(`/users/${id}`)
//   dispatch({ type: 'FETCH_USER', payload: resp.data })
// })

// dont do this! breaking rules of an action creator
// need to return plan JS Objects, not async function request
//must have type, optional payload
// redux thunk allows action object or functions
// export const fetchPosts = async () => {
//     const resp = await json.get('/posts')
//     return { type: 'FETCH_POSTS', payload: resp }
//   }
