# React Redux

Projects from the course Modern React and Redux
https://www.udemy.com/course/react-redux/

## Installation

npm i

## Usage

This is for learning only, please do not use the code in commerical projects

## Support

Email justinbarsketis@gmail.com

## Contributing

This is a closed project

## Projects

- jsx - Differences between html and jsx, basic styling
- components-basic - Writing basic components, props, component reuse, children
- class-based-components - Getting users location example using class based components
- userinput-events-search - Image Search application, Handing user data, events, forms, basic api using axios.
- modern-hooks-widget - An app to practice using hooks by making custom widgets. Made a custom accordian component, a search function of wikipedia, a simple dropdown menu, and integrates a custom navigation menu. Uses basic hook states, use effect, debouncing, and api calls.
- youtube-video-search - An app that searches youtube api and returns a mock youtube experience with a search bar. api key removed. Uses custom hooks.
- basic-redux-songs - Basic app to select a song and show the details on the right. Uses redux including redux, reducers, actions
- async-redux-thunk-actions - More advanced app that connects to a fake api to grab a blog list. Uses redux thunk basic features plus middleware, lodash and memoize to prevent overfetching
- steaming-app-redux-crud-auth - Final large project that is a streaming app that implements a CRUD api using redux. Implements google auth, redxu, redux dev tools, implement basic api server, redux thunk, reducers using objects instead of arrays, Modal using portals, router wildcars, and rmtp stream integration.
- language-context-api - Simple app that changes button language using react context api
  -language-context-api-redux-style - more complex integration that shows how you could use context in repacement for redux by implementing a store and selector.

## License

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
