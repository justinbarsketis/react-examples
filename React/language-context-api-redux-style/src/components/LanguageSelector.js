import React from 'react'
import LanguageContext from '../contexts/LanguageContext'

// only need to access 1 context so using this.context
class LanguageSelector extends React.Component {
  static contextType = LanguageContext

  render() {
    return (
      <div>
        Select a Language
        <i
          onClick={() => this.context.onLanguageChange('english')}
          className="flag us"
        />
        <i
          onClick={() => this.context.onLanguageChange('dutch')}
          className="flag nl"
        />
      </div>
    )
  }
}

export default LanguageSelector
