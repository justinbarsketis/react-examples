import axios from 'axios'

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 'Client-ID pTlLGMZufO3ybtWU_9eB3iZJtJHk2c30pLLU5vk34sA'
  }
})
