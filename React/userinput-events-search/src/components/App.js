import React from 'react'
import unsplash from '../api/unsplash'
import SearchBar from './SearchBar'
import ImageList from './ImageList'

class App extends React.Component {
  state = { images: [] }
  onSearchSubmit = async (term) => {
    const response = await unsplash.get('/search/photos', {
      params: { query: term }
    })
    this.setState({ images: response.data.results })

    // promise axios example
    // onSearchSubmit(term) {
    //   axios
    //     .get('https://api.unsplash.com/search/photos', {
    //       params: { query: term },
    //       headers: {
    //         Authorization: 'Client-ID pTlLGMZufO3ybtWU_9eB3iZJtJHk2c30pLLU5vk34sA'
    //       }
    //     })
    //     .then((response) => {
    //       console.log(response.data.results)
    //     })
  }
  render() {
    return (
      <div className="ui container" style={{ marginTop: 25 }}>
        <SearchBar onSubmit={this.onSearchSubmit} />
        <ImageList images={this.state.images} />
      </div>
    )
  }
}

export default App

// pTlLGMZufO3ybtWU_9eB3iZJtJHk2c30pLLU5vk34sA
// 1ciWybXBVeY_PXcoaaMhqDALgYhE5HGiIFZTU47k-qg
