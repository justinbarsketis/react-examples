# React Native - Basic Examples

This is a React Native project that shows you the basics of:

- Restaurant App - Yelp Restaurant Search App, putting it all together with state and API calls

React Navigator Practice, icons, third party apis, network error handling, useEffect and custom hooks. navigator, custom details screen, empty element spacing, hiding scroll bar, comminicating from navigator component, fetching images

- - Restaurant-search-app

## Installation

npm i

npm run start

## Usage

This is for learning only, please do not use the code in commerical projects

## Support

Email justinbarsketis@gmail.com

## Contributing

This is a closed project

## Authors and acknowledgment

Justin Barsketis

React Native Module 1-5 - https://www.udemy.com/course/the-complete-react-native-and-redux-course/

## License

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
