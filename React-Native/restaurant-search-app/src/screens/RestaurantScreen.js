import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import yelp from '../api/yelp'

// nvaigate doesnt use props directly
const RestaurantScreen = ({ navigation }) => {
  const [restaurant, setRestaurant] = useState(null) // if empty object, use null not empty object

  const id = navigation.getParam('id')
  const getTestaurant = async (id) => {
    try {
      const resp = await yelp.get(`/${id}`)
      setRestaurant(resp.data)
    } catch (error) {}
  }
  useEffect(() => {
    getTestaurant(id)
  }, [])

  if (!restaurant) {
    return null
  }
  return (
    <View>
      <Text>{restaurant.name}</Text>
      <FlatList
        data={restaurant.photos}
        keyExtractor={(photo) => photo}
        renderItem={({ item }) => {
          return <Image style={styles.image} source={{ uri: item }} />
        }}
      ></FlatList>
    </View>
  )
}
const styles = StyleSheet.create({
  image: {
    height: 200,
    width: 300
  }
})
export default RestaurantScreen
