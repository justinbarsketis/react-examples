import React, { useState } from 'react'
import { View, Text, StyleSheet, ScrollView } from 'react-native'
import SearchBar from '../components/SearchBar'
import useRestaurants from '../hooks/useRestaurants'
import RestaurantList from '../components/RestaurantList'

const SearchScreen = () => {
  const [term, setTerm] = useState('')
  const [searchApi, restaurants, errorMessage] = useRestaurants()

  const filterResultsByPrice = (price) => {
    return restaurants.filter((restaurant) => {
      return restaurant.price === price
    })
  }

  return (
    // <View style={{ flex: 1 }}>
    <>
      {/* if content is cut off, adding flex 1 to only fill visible space */}
      {/* you can also use a fragment or empty object <></> */}
      <SearchBar
        term={term}
        onTermChange={setTerm}
        onTermSubmit={() => searchApi(term)}
      />
      {errorMessage ? <Text>{errorMessage}</Text> : null}
      <ScrollView>
        <RestaurantList
          restaurants={filterResultsByPrice('$')}
          title="Budget"
        />
        <RestaurantList
          restaurants={filterResultsByPrice('$$')}
          title="Good Eats"
        />
        <RestaurantList
          restaurants={filterResultsByPrice('$$$')}
          title="Fine Dining"
        />
      </ScrollView>
    </>
  )
}

const style = StyleSheet.create({})

export default SearchScreen
