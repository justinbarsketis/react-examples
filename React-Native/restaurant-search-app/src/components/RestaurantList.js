import React from 'react'
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity
} from 'react-native'
import { withNavigation } from 'react-navigation'
import RestaurantDetail from './RestaurantDetail'

const RestaurantList = ({ title, restaurants, navigation }) => {
  if (!restaurants.length) {
    return null
  }
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <FlatList
        horizontal
        data={restaurants}
        keyExtractor={(restaurant) => {
          return restaurant.id
        }}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              onPress={() => navigation.navigate('Restaurant', { id: item.id })}
            >
              <RestaurantDetail restaurant={item} />
            </TouchableOpacity>
          )
        }}
      />
    </View>
  )
}
const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 10,
    marginBottom: 5
  },
  container: {
    marginBottom: 10
  }
})

export default withNavigation(RestaurantList) // instead of passing navigation from parent, use with navigation
