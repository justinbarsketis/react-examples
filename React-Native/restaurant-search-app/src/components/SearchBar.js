import React from 'react'
import { View, Text, TextInput, StyleSheet } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

const SearchBar = ({ term, onTermChange, onTermSubmit }) => {
  return (
    <View style={styles.backgroundStyle}>
      <Ionicons
        onPress={onTermSubmit}
        name="search-outline"
        style={styles.iconStyle}
      />
      <TextInput
        autoCapitalize="none"
        autoCorrect={false}
        style={styles.inputStyle}
        placeholder="Search"
        value={term}
        // onEndEditing={() => {
        //   onTermSubmit()
        // }} // once they hit the submit button on phone
        // onChangeText={(newTerm) => onTermChange(newTerm)}
        onEndEditing={onTermSubmit} // same as above
        onChangeText={onTermChange}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  backgroundStyle: {
    backgroundColor: '#F0EEEE',
    height: 50,
    borderRadius: 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10
  },
  inputStyle: {
    flex: 1,
    fontSize: 18
  },
  iconStyle: {
    fontSize: 40,
    color: 'black',
    alignSelf: 'center'
  }
})

export default SearchBar
