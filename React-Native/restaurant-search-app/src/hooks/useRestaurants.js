// hook adds additional functionality to a component that we create
// tip remove all the code related to the api from the main component first
// return the variables that you need in the parent jsx, in an array
// then import hook to parent
import { useEffect, useState } from 'react'
import yelp from '../api/yelp'

export default () => {
  const [restaurants, setRestaurants] = useState([])
  const [errorMessage, setErrorMessage] = useState('')

  const searchApi = async (searchTerm) => {
    try {
      const resp = await yelp.get('/search', {
        params: {
          limit: 50,
          term: searchTerm,
          location: 'san miguel de allende, mexico'
        }
      }) // params automatically appended to url
      setRestaurants(resp.data.businesses)
      // setErrorMessage('')
    } catch (error) {
      console.log(error)
      setErrorMessage('Something went wrong')
    }
  }
  useEffect(() => {
    searchApi('Mexican')
  }, [])

  return [searchApi, restaurants, errorMessage]
}
