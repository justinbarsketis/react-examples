# React Native - Basic Examples

This is a React Native project that shows you the basics of:

- Components, JSX, Styling
- - ComponentsScreen.js

* List Components, Key Usage, Key Extrator

- - FlatLists ListScreen.js

* Basic Naviation, Buttons, TouchableOpacity

- - HomeScreen.js

* Custom Components, Images, Urls, and Props

- - ProviderDetail.js, ProviderComponentScreen.js

* State, Set State, Example Counter, Add User Custom State Example

- - CounterScreen.js, UserDobScreen.js

* Reusable components with state, Passing Callback Functions, Child Parent State Relationships, Validating State, Ternary statment

- - SquareScreen.js, ColorCounter.js

* Reducers - Reducer, Dispatch, Action Types, Payloads

- - SquareScreenReducer.js CounterScreenReducerExercise.js

* Text State Handling, password verification example

- - TextScreenState.js

* Layout Box Screen - Basic CSS used to style apps

- - BoxScreen.js

If you get lost, you can take a look at the pushes, you can see specific code pushes related to the topics above as I completed each module. The project is slightly different from the course to have more real life applications for my specific work projects.

## Installation

npm i

npm run start

## Usage

This is for learning only, please do not use the code in commerical projects

## Support

Email justinbarsketis@gmail.com

## Contributing

This is a closed project

## Authors and acknowledgment

Justin Barsketis

React Native Module 1-5 - https://www.udemy.com/course/the-complete-react-native-and-redux-course/

## License

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
