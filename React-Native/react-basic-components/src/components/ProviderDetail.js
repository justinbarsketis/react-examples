import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'

const ProviderDetail = (props) => {
  return (
    <View>
      <Text>{props.provider}</Text>
      <Image style={styles.imgWidth} source={props.imageUrl} />
    </View>
  )
}

const styles = StyleSheet.create({
  imgWidth: {
    height: '33%',
    width: '100%'
  }
})

export default ProviderDetail
