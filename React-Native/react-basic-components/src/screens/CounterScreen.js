import React, { useState } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'

const CounterScreen = () => {
  const [cost, setCost] = useState(0)

  return (
    <View>
      <Button
        title="Calculate"
        onPress={() => {
          setCost(cost + 1000)
        }}
      />
      <Button
        title="Double"
        onPress={() => {
          setCost(cost * 2)
        }}
      />
      <Button
        title="Clear"
        onPress={() => {
          setCost(0)
        }}
      />
      <Text>The current cost is: ${cost}</Text>
    </View>
  )
}

const styles = new StyleSheet.create({})
export default CounterScreen
