import React from 'react'
import { Text, View, StyleSheet } from 'react-native'

const BoxScreen = () => {
  return (
    <View style={styles.viewStyle}>
      <Text style={styles.text1Style}>Box Screen 1</Text>
      <Text style={styles.text2Style}>Box Screen 2</Text>
      <Text style={styles.text3Style}>Box Screen 3</Text>
      <Text style={styles.text4Style}>Box Screen 4</Text>
      <Text style={styles.text5Style}>Box Screen 5</Text>
      <Text style={styles.text6Style}>Modal 6</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  viewStyle: {
    borderWidth: 1,
    borderColor: 'black',
    height: 300
    // margin: 10,
    // core flex box styles on parent elements
    // alignItems: 'stretch // default, needs to be on parent element, children strech out
    // alignItems: 'flex-start' floats left
    // alignItems: 'center',
    // alignItems: 'flex-end', floats right
    // flexDirection: 'column' // default, lay out divs vertically
    // flexDirection: 'row', // lay out divs horizontalls
    // justifyContent: 'flex-start' // default, align top of box
    // justifyContent: 'center' // default, align center of box
    // justifyContent: 'flex-end' // default, align bottom of box
    // justifyContent: 'space-between' // default, equal space between
    //     justifyContent: 'space-around' // default, equal space, on top and bottom elements as well
  },
  text1Style: {
    borderWidth: 2,
    borderColor: 'red',
    flex: 2, // 3 + 1 + 6 + 10 === 15% of space
    alignSelf: 'flex-end' // overrides parent align items
  },
  text2Style: {
    borderWidth: 2,
    borderColor: 'green',
    flex: 1 // on only 1 element will take up as much as possible if others have no flex
  },
  text3Style: {
    borderWidth: 2,
    borderColor: 'blue',
    flex: 6 // 3 + 1 + 6 === 30% of space
  },
  text4Style: {
    borderWidth: 2,
    borderColor: 'yellow',
    position: 'absolute'
    // absolute ignores other elements, but will position relative dpeneding on certain box properties
  },
  text5Style: {
    borderWidth: 2,
    borderColor: 'purple',
    flex: 10, // 3 + 1 + 6 === 50% of space
    left: 10 // top bottom etc adjust after it has been laid out
  },
  text6Style: {
    // trick to fill enitre parent
    borderWidth: 5,
    borderColor: 'orange',
    position: 'absolute',
    // left: 0,
    // top: 0,
    // right: 0,
    // bottom: 0
    ...StyleSheet.absoluteFillObject // equal to above
  }
})

export default BoxScreen
