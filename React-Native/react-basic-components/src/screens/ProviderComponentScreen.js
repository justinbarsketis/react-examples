import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import ProviderDetail from '../components/ProviderDetail'

const ProviderComponentScreen = () => {
  return (
    <View>
      <ProviderDetail
        provider="Cigna"
        imageUrl={require('../../assets/logos/cigna.jpg')}
      />
      <ProviderDetail
        provider="IMG"
        imageUrl={require('../../assets/logos/img.png')}
      />
      <ProviderDetail
        provider="Geoblue"
        imageUrl={require('../../assets/logos/geoblue.png')}
      />
    </View>
  )
}

const styles = StyleSheet.create({})

export default ProviderComponentScreen
