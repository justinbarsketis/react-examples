import React from 'react'
import { View, Button, Text, StyleSheet, TouchableOpacity } from 'react-native'

const HomeScreen = ({ navigation }) => {
  return (
    <View>
      <Text style={styles.text}>Expat Insurance</Text>
      <Button onPress={() => navigation.navigate('Home')} title="Home" />
      <Button
        onPress={() => navigation.navigate('Components')}
        title="Component"
      />
      <Button
        onPress={() => navigation.navigate('List')}
        title="List Component"
      />
      <Button
        onPress={() => navigation.navigate('Provider')}
        title="Custom Image Component"
      />
      <Button
        onPress={() => navigation.navigate('Counter')}
        title="Custom State Component"
      />
      <Button
        onPress={() => navigation.navigate('UserDob')}
        title="User Custom State Component"
      />
      <Button
        onPress={() => navigation.navigate('Square')}
        title="Reusable State Component"
      />
      <Button
        onPress={() => navigation.navigate('SquareReducer')}
        title="Reusable State Component w/ Reducer"
      />
      <Button
        onPress={() => navigation.navigate('CounterReducer')}
        title="Reusable State Component w/ Reducer Exercise"
      />
      <Button
        onPress={() => navigation.navigate('TextScreen')}
        title="Text Screen State"
      />
      <Button
        onPress={() => navigation.navigate('Box')}
        title="Box Style Demo"
      />
      <TouchableOpacity onPress={() => navigation.navigate('List')}>
        <Text>Policies List Opacity Example</Text>
        <Text>Policies List</Text>
        <Text>Policies List</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
})

export default HomeScreen
