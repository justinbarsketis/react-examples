import React, { useReducer } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'

const reducer = (state, action) => {
  //state = cost: number
  //action = type: increase_cost, double_cost, clear_cost, payload: +1000 || * 2 || 0

  switch (action.type) {
    case 'increase_cost':
      return { ...state, cost: state.cost + action.payload }
    case 'double_cost':
      return { ...state, cost: state.cost * action.payload }
    case 'clear_cost':
      return { ...state, cost: state.cost * action.payload }
    default:
      return state
  }
}

const CounterScreenReducerExercise = () => {
  const [state, dispatch] = useReducer(reducer, { cost: 0 })
  const { cost } = state

  return (
    <View>
      <Button
        title="Calculate"
        onPress={() => {
          dispatch({ type: 'increase_cost', payload: 1000 })
        }}
      />
      <Button
        title="Double"
        onPress={() => {
          dispatch({ type: 'double_cost', payload: 2 })
        }}
      />
      <Button
        title="Clear"
        onPress={() => {
          dispatch({ type: 'clear_cost', payload: 0 })
        }}
      />
      <Text>The current cost is: ${cost}</Text>
    </View>
  )
}

const styles = new StyleSheet.create({})
export default CounterScreenReducerExercise
