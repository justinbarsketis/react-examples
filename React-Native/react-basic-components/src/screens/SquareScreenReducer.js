import React, { useReducer } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import ColorCounter from '../components/ColorCounter'
const INCREMENT_COUNTER = 10
const reducer = (state, action) => {
  // state = {red: number, green: number, blue: number}
  // action === {type: 'change_red' || colorToChange: 'change_green' || colorToChange: 'change_blue', payload: 15 || -15}
  switch (action.type) {
    case 'change_red':
      return state.red + action.payload > 255 || state.red + action.payload < 0
        ? state
        : { ...state, red: state.red + action.payload }
    case 'change_green':
      return state.green + action.payload > 255 ||
        state.green + action.payload < 0
        ? state
        : { ...state, green: state.green + action.payload }
    case 'change_blue':
      return state.blue + action.payload > 255 ||
        state.blue + action.payload < 0
        ? state
        : { ...state, blue: state.blue + action.payload }
    default:
      return state
  }
}
const SquareScreenReducer = () => {
  const [state, dispatch] = useReducer(reducer, { red: 0, green: 0, blue: 0 })
  const { red, green, blue } = state
  return (
    <View>
      <ColorCounter
        onIncrease={() =>
          dispatch({ type: 'change_red', payload: INCREMENT_COUNTER })
        }
        onDecrease={() =>
          dispatch({ type: 'change_red', payload: -1 * INCREMENT_COUNTER })
        }
        color="Red"
      />
      <ColorCounter
        onIncrease={() =>
          dispatch({ type: 'change_green', payload: INCREMENT_COUNTER })
        }
        onDecrease={() =>
          dispatch({ type: 'change_green', payload: -1 * INCREMENT_COUNTER })
        }
        color="Green"
      />
      <ColorCounter
        onIncrease={() =>
          dispatch({ type: 'change_blue', payload: INCREMENT_COUNTER })
        }
        onDecrease={() =>
          dispatch({ type: 'change_blue', payload: -1 * INCREMENT_COUNTER })
        }
        color="Blue"
      />
      <View
        style={{
          width: 150,
          height: 150,
          backgroundColor: `rgb(${red},${green},${blue})`
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({})

export default SquareScreenReducer
