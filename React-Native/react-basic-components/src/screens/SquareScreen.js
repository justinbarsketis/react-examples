import React, { useState } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import ColorCounter from '../components/ColorCounter'

const SquareScreen = () => {
  const [red, setRed] = useState(0)
  const [green, setGreen] = useState(0)
  const [blue, setBlue] = useState(0)
  const INCREMENT_COUNTER = 10

  const setColor = (color, change) => {
    switch (color) {
      case 'red':
        red + change > 255 || red + change < 0 ? null : setRed(red + change) // ternary statement, return null, dont do anything
        return
      case 'green':
        green + change > 255 || green + change < 0
          ? null
          : setGreen(green + change)
        return
      case 'blue':
        blue + change > 255 || blue + change < 0 ? null : setBlue(blue + change)
      default:
        return
    }
  }
  return (
    <View>
      <ColorCounter
        onIncrease={() => setColor('red', INCREMENT_COUNTER)}
        onDecrease={() => setColor('red', INCREMENT_COUNTER * -1)}
        color="Red"
      />
      <ColorCounter
        onIncrease={() => setColor('green', INCREMENT_COUNTER)}
        onDecrease={() => setColor('green', INCREMENT_COUNTER * -1)}
        color="Green"
      />
      <ColorCounter
        onIncrease={() => setColor('blue', INCREMENT_COUNTER)}
        onDecrease={() => setColor('blue', INCREMENT_COUNTER * -1)}
        color="Blue"
      />
      <View
        style={{
          width: 150,
          height: 150,
          backgroundColor: `rgb(${red},${green},${blue})`
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({})

export default SquareScreen
