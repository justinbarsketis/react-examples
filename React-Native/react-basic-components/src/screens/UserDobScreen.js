import React, { useState } from 'react'
import { View, Text, StyleSheet, Button, FlatList } from 'react-native'

const UserDobScreen = () => {
  const [dobs, setDobs] = useState([])

  return (
    <View>
      <Button
        title="Add a user"
        onPress={() => {
          setDobs([...dobs, randomDob()])
        }}
      />
      <Button
        title="Clear"
        onPress={() => {
          setDobs([])
        }}
      />
      <FlatList
        keyExtractor={(item) => item} // bad key, is possible it would not be unique but fine for example
        data={dobs}
        renderItem={({ item }) => {
          return (
            <View style={{ height: 50, width: '75%' }}>
              <Text>The user date of birth is: {item}</Text>
            </View>
          )
        }}
      />
    </View>
  )
}
const randomDob = () => {
  const month = Math.floor(Math.random() * 12 + 1)
  const day = Math.floor(Math.random() * 30 + 1)
  const year = Math.floor(Math.random() * 100 + 1900)

  return `${month}/${day}/${year}`
}

const styles = new StyleSheet.create({})
export default UserDobScreen
