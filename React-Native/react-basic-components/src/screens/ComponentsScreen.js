import React from 'react'
import { Text, StyleSheet, View } from 'react-native'

const ComponentsScreen = () => {
  const companyName = 'Expat Insurance'
  const subTitle = 'Safeguard your Journey'
  return (
    <View>
      <Text style={styles.textStyle}>Welcome to {companyName}</Text>
      <Text style={styles.subStyle}>{subTitle}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 36
  },
  subStyle: {
    fontSize: 28
  }
})

export default ComponentsScreen
