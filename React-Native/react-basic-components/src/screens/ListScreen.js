import React from 'react'
import { Text, StyleSheet, View, FlatList } from 'react-native'

const listScreen = () => {
  const policies = [
    { policyName: 'Max', provider: 'Cigna' },
    { policyName: 'Flex', provider: 'Cigna' },
    { policyName: 'Platinum', provider: 'BMI' },
    { policyName: 'Great', provider: 'IMG' },
    { policyName: 'Good', provider: 'IMG' },
    { policyName: 'Meh', provider: 'IMG' },
    { policyName: 'Super', provider: 'Trawick' },
    { policyName: 'Insane', provider: 'VUMI' }
  ]
  return (
    <FlatList
      //horizontal // remove to make vertical list
      showsHorizontalScrollIndicator={false}
      keyExtractor={(policy) => policy.provider + policy.policyName} // unique key in case duplicate provider keys
      data={policies}
      renderItem={({ item }) => {
        return (
          <Text style={styles.textStyle}>
            {item.provider} - {item.policyName}
          </Text>
        )
      }}
    />
  )
}
const styles = StyleSheet.create({
  textStyle: {
    marginVertical: 25,
    fontSize: 36
  },
  subStyle: {
    fontSize: 28
  }
})

export default listScreen
