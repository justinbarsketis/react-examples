import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import HomeScreen from './src/screens/HomeScreen'
import ComponentsScreen from './src/screens/ComponentsScreen'
import ListScreen from './src/screens/ListScreen'
import ProviderComponentScreen from './src/screens/ProviderComponentScreen'
import CounterScreen from './src/screens/CounterScreen'
import UserDobScreen from './src/screens/UserDobScreen'
import SquareScreen from './src/screens/SquareScreen'
import SquareScreenReducer from './src/screens/SquareScreenReducer'
import CounterScreenReducerExercise from './src/screens/CounterScreenReducerExercise'
import TextScreen from './src/screens/TextScreenState'
import BoxScreen from './src/screens/BoxScreen'

const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    Components: ComponentsScreen,
    List: ListScreen,
    Provider: ProviderComponentScreen,
    Counter: CounterScreen,
    UserDob: UserDobScreen,
    Square: SquareScreen,
    SquareReducer: SquareScreenReducer,
    CounterReducer: CounterScreenReducerExercise,
    TextScreen: TextScreen,
    Box: BoxScreen
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      title: 'App'
    }
  }
)

export default createAppContainer(navigator)
