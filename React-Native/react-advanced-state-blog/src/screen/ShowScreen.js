import React, { useContext } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Context } from '../context/BlogContext'
import { MaterialCommunityIcons } from '@expo/vector-icons'

const ShowScreen = ({ navigation }) => {
  const { state } = useContext(Context)
  const id = navigation.getParam('id')
  const post = state.find((post) => {
    return post.id === id
  })
  return (
    <View>
      <Text>{post.title}</Text>
    </View>
  )
}
ShowScreen.navigationOptions = ({ navigation }) => {
  return {
    headerRight: () => (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('Edit', { id: navigation.getParam('id') })
        }
      >
        <MaterialCommunityIcons name="pencil" size={24} color="black" />
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({})

export default ShowScreen
