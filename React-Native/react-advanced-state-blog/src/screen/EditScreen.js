import React, { useState, useContext } from 'react'
import { View, Text, StyleSheet, TextInput } from 'react-native'
import { Context } from '../context/BlogContext'

const EditScreen = ({ navigation }) => {
  const { state } = useContext(Context)
  const id = navigation.getParam('id')
  const post = state.find((post) => {
    return post.id === id
  })
  const [title, setTitle] = useState(post.title)
  const [content, setContent] = useState(post.content)
  return (
    <View>
      <TextInput
        style={styles.input}
        value={title}
        onChangeText={(text) => setTitle(text)}
      />
      <Text style={styles.label}>Enter Content</Text>
      <TextInput
        style={styles.input}
        value={content}
        onChangeText={(text) => setContent(text)}
      />
      <Button
        onPress={
          () => addBlogPost(title, content, () => navigation.navigate('Index')) // passing navigation function to be done in case there is a delay creating post like an api
        }
        title="Add Blog Post"
      />
    </View>
  )
}



export default EditScreen
