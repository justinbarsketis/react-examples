import React, { useState, useContext } from 'react'
import { View, Text, StyleSheet, TextInput, Button } from 'react-native'
import { Context } from '../context/BlogContext'

const BlogForm = ({ navigation }) => {

  const [title, setTitle] = useState(post.title)
  const [content, setContent] = useState(post.content)
  return (
    <View>
      <TextInput
        style={styles.input}
        value={title}
        onChangeText={(text) => setTitle(text)}
      />
      <Text style={styles.label}>Enter Content</Text>
      <TextInput
        style={styles.input}
        value={content}
        onChangeText={(text) => setContent(text)}
      />
      <Button
        // onPress={
        //   () => addBlogPost(title, content, () => navigation.navigate('Index')) // passing navigation function to be done in case there is a delay creating post like an api
        // }
        title="Save Blog Post"
      />
    </View>
  )
}

const styles = StyleSheet.create({
    input: {
      fontSize: 18,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 15,
      padding: 5,
      margin: 5
    },
    label: {
      fontSize: 20,
      marginBottom: 5,
      marginLeft: 5
    }
  })
  

export default BlogForm
