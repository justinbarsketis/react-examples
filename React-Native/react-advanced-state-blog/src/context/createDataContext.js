// when exporting plain functions, use a lowerCase convention
import React, { useReducer } from 'react'

// generic reusable function automate the process of setting up context
export default (reducer, actions, initialState) => {
  const Context = React.createContext()

  const Provider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState)

    // actions is an object with the key like addBlog post, that is a function with dispatch, that returns a function that actually does something
    // actions === {addBlogPost: (dispatch) => {return () => {} } }
    const boundActions = {}
    for (let key in actions) {
      boundActions[key] = actions[key](dispatch)
    }
    return (
      <Context.Provider value={{ state, actions, ...boundActions }}>
        {children}
      </Context.Provider>
    )
  }

  return { Context, Provider }
}
