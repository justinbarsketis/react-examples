import React, { useReducer } from 'react'
import createDataContext from './createDataContext'

const blogReducer = (state, action) => {
  switch (action.type) {
    case 'add_blogpost':
      return [
        ...state,
        {
          id: Math.floor(Math.random() * 99999),
          title: action.payload.title,
          content: action.payload.content
        }
      ]
    case 'remove_blogpost':
      return state.filter((post) => {
        return post.id !== action.payload
      })
    default:
      return state
  }
}

const addBlogPost = (dispatch) => {
  return (title, content, callback) => {
    dispatch({ type: 'add_blogpost', payload: { title, content } })
    callback()
  }
}
const removeBlogPost = (dispatch) => {
  return (id) => {
    dispatch({ type: 'remove_blogpost', payload: id })
  }
}

export const { Context, Provider } = createDataContext(
  blogReducer,
  { addBlogPost, removeBlogPost },
  [{ title: 'Demo Post', content: 'Test content', id: 1 }]
)

// Code not using the context
// const BlogContext = React.createContext()

// const blogReducer = (state, action) => {
//   switch (action.type) {
//     case 'add_blogpost':
//       return [...state, { title: `Blog Post #${state.length + 1}` }]
//     default:
//       return state
//   }
// }

// export const BlogProvider = ({ children }) => {
//   const [blogPosts, dispatch] = useReducer(blogReducer, [])

//   const addBlogPost = () => {
//     dispatch({ type: 'add_blogpost' })
//   }

//   return (
//     <BlogContext.Provider value={{ data: blogPosts, addBlogPost }}>
//       {children}
//     </BlogContext.Provider>
//   )
// }

// export default BlogContext
