// without interface example
const oldCivic = {
  model: 'civic',
  year: 2000,
  broken: true
}

const printVehicle = (vehicle: {
  model: string
  year: number
  broken: boolean
}): void => {
  console.log(`
    ${vehicle.model}
    ${vehicle.year}
    ${vehicle.broken}`)
}

printVehicle(oldCivic)

interface Vehicle {
  model: string
  year: number
  broken: boolean
  created: Date
  summary(): string // what it returns
}

const oldCivic2 = {
  model: 'civic',
  year: 2000,
  broken: true,
  created: new Date(),
  summary(): string {
    return `${this.model}`
  }
}

const printVehicle2 = (vehicle: Vehicle): void => {
  console.log(vehicle.summary())
}

interface Reportable {
  summary(): string // just an interface that reportable or printable, can be reusable
}

const printSummary = (item: Reportable): void => {
  console.log(item.summary())
}

printSummary(oldCivic2)

const drink2 = {
  color: 'brown',
  carbonated: true,
  sugar: 40,
  summary(): string {
    return `${this.color}`
  }
}

printSummary(drink2)
// we can reuse the code now
