import 'reflect-metadata'

// const plane = {
//   color: 'red'
// }

// Reflect.defineMetadata('note', 'hi there', plane)
// const note = Reflect.getMetadata('note', plane)
// Reflect.defineMetadata('height', 10, plane)

// Reflect.defineMetadata('test', 'asd', plane, 'color') // on the color object
// const test = Reflect.getMetadata('test', plane, 'color')

@controller
class Plane {
  color: string = 'red'

  @get('hello')
  fly(): void {
    console.log('vrrrrr')
  }
}

function get(path: string) {
  return function (target: Plane, key: string) {
    Reflect.defineMetadata('path', path, target, key)
  }
}

// const secret = Reflect.getMetadata('secret', Plane.prototype, 'fly')
// console.log(secret)

function controller(target: typeof Plane) {
  for (let key in target.prototype) {
    const path = Reflect.getMetadata('path', target.prototype, key)
    console.log(path)
  }
}
