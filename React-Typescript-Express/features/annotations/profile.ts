const profile = {
  name: 'justin',
  age: 29,
  coords: {
    lat: 0,
    lon: 24
  },
  setAge(age: number): void {
    this.age = age
  }
}

const { age }: { age: number } = profile

// const {coords: {lat, lon}} = profile
const {
  coords: { lat, lon }
}: { coords: { lat: number; lon: number } } = profile
