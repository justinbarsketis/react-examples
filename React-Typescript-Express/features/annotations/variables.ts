const apples: number = 5
let speed: string = 'fast'
let hasName: boolean = true

let nothing: null = null
let nothingSet: undefined = undefined

// built in objects
let now: Date = new Date()

// array
let color: string[] = ['red', 'blue']
let numbers: number[] = [1, 2, 3, 4]

// classes
class Car {}

let car: Car = new Car()

// Object literal
let point: { x: number; y: number } = {
  x: 10,
  y: 20
}

// function
const logNumber: (i: number) => void = (i: number) => {
  console.log(i)
}

// type any, do your best to avoid type any
const json = '{"x":10,"y":20}'
const coordinates = JSON.parse(json)
console.log(coordinates)

// how to fix a function returning type any
const json1 = '{"x":10,"y":20}'
const coordinates1: { x: number; y: number } = JSON.parse(json1)
console.log(coordinates1)

// other any example, initialize on seperate lines
// let foundWord: boolean
let foundWord
let words = ['red', 'green']
for (let i = 0; i < words.length; i++) {
  foundWord = true
}

// variable that cannot be inferred correctly
let numbers1 = [-10, 1, 12]
let numberAboveZero: boolean | number = false // can be either a boolean or number

for (let i = 0; i < numbers1.length; i++) {
  if (numbers1[i] > 0) {
    numberAboveZero = numbers1[i]
  }
}

// returns a number
const add = (a: number, b: number): number => {
  return a + b
}
// using type inference for the return
const add1 = (a: number, b: number) => {
  return a + b
}
// example why you want to declare a return, it is not showing this as an error even though we expect a return
const subtract = (a: number, b: number) => {
  a - b
}
// function example
function divide(a: number, b: number): number {
  return a / b
}

// anonymous function
const multiply = function (a: number, b: number): number {
  return a * b
}

// wont return anything, or null, or undefined
const logger = (message: string): void => {
  console.log(message)
}

// we will never reach the end of the function
const throwErrpr = (message: string): never => {
  throw new Error(message)
}

const todaysWeather = {
  date: new Date(),
  weather: 'sunny'
}

// how to use deconstruction
const logWeather = (forecast: { date: Date; weather: string }): void => {
  console.log(forecast.date)
  console.log(forecast.weather)
}
logWeather(todaysWeather)

const logWeather1 = ({
  date,
  weather
}: {
  date: Date
  weather: string
}): void => {
  console.log(date)
  console.log(weather)
}
logWeather1(todaysWeather)
