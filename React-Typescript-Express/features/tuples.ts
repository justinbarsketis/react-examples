const drink = { color: 'brown', carbonated: true, sugar: 40 }

// notice the order is not exact here
const pepsi = ['brown', true, 40]

// this is now a tuple
const coke: [string, boolean, number] = ['brown', true, 50]

// how to make the tuple
type Drink = [string, boolean, number]
const sprite: Drink = ['clear', true, 50]

// why he doesnt like tuples
const carSpecs: [number, number] = [400, 3354]
// what are the numbers?

const carStats = { horsepower: 400, weight: 3354 }
// this is much easier to read
