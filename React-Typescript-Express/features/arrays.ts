const carMakers = ['Jeep', 'Mercedes', 'Toyota']
// type inferenced

// if not initializing the array
const anyCarMakers: string[] = []

const dates = [new Date(), new Date()]

// 2d array
const carsByMake = [['f150'], ['asdas'], ['asdasd']]

// annotated
const anyCarsByMake: string[][] = []

// help with inference when extracting values
const carString = carMakers[0]
const carString1 = carMakers.pop()

// return string
carMakers.map((car: string): string => {
  return car
})

// inference or like prior example Date | String
const importantDates = [new Date(), '2030-10-10']
const importantDates2: (Date | string)[] = [new Date(), '2030-10-10']
