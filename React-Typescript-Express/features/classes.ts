class Vehicle {
  color: string = 'red' // initial color
  model: string
  constructor(model: string) {
    this.model = model
  }
  drive(): void {
    console.log('drive')
  }
}

const vehicle = new Vehicle('si')
vehicle.drive()

class Car1 extends Vehicle {
  drive(): void {
    console.log('vroom')
  }
}

const car2 = new Car1('schwifty')
car2.drive()
