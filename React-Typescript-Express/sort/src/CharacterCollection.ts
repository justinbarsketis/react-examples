import { Sorter } from './Sorter'
export class CharacterCollection extends Sorter {
  constructor(public data: string) {
    super()
  }

  compare(leftIndex: number, rightIndex: number): boolean {
    return (
      this.data[leftIndex].toLocaleLowerCase() >
      this.data[rightIndex].toLocaleLowerCase()
    )
  }
  swap(leftIndex: number, rightIndex: number): void {
    const characters = this.data.split('')
    const temp = characters[leftIndex]
    characters[leftIndex] = characters[rightIndex]
    characters[rightIndex] = temp
    this.data = characters.join('')
  }
  get length(): number {
    return this.data.length
  }
}
