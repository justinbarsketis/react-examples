import { NumbersCollection } from './NumbersCollection'
import { CharacterCollection } from './CharacterCollection'
import { LinkedList } from './LinkedList'

const numbersCollection = new NumbersCollection([-4, 1, 0, 12, 3])
numbersCollection.sort()
console.log(numbersCollection.data)

const characterCollection = new CharacterCollection('foSkdFDsjQ')
characterCollection.sort()
console.log(characterCollection.data)

const linkedList = new LinkedList()

linkedList.add(300)
linkedList.add(30)
linkedList.add(3)
linkedList.add(99)
linkedList.add(23)

linkedList.sort()
console.log(linkedList.print())
