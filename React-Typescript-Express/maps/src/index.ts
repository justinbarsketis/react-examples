/// <reference types="@types/google.maps" />
import { User } from './User'
import { Company } from './Company'

const user = new User()
const company = new Company()

// console.log(user)
// console.log(company)

import { Map } from './Map'

const customMap = new Map('map')
customMap.addMarker(user)
customMap.addMarker(company)
