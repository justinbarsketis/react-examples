import faker from 'faker'

export class User {
  name: string
  location: {
    lat: number
    lng: number
  }
  constructor() {
    this.name = faker.name.firstName()
    this.location = {
      lat: parseFloat(faker.address.latitude()),
      lng: parseFloat(faker.address.longitude())
    }
  }
  content(): string {
    return `
      Name is: ${this.name}
      Location is: ${this.location.lat} - ${this.location.lng}
      `
  }
}
