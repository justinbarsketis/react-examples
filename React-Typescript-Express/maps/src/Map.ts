interface Mappable {
  location: {
    lat: number
    lng: number
  }
  content(): string
}

export class Map {
  private googleMap: google.maps.Map // we dont want others to access this
  constructor(id: string) {
    this.googleMap = new google.maps.Map(document.getElementById(id), {
      zoom: 1,
      center: { lat: 0, lng: 0 }
    })
  }

  addMarker(mappable: Mappable): void {
    const marker = new google.maps.Marker({
      map: this.googleMap,
      position: { lat: mappable.location.lat, lng: mappable.location.lng }
    })
    marker.addListener('click', () => {
      const infoWindow = new google.maps.InfoWindow({
        content: mappable.content()
      })
      infoWindow.open(this.googleMap, marker)
    })
  }
}
